import { ResultStatus } from './enums';

export interface IUser {
    name: {
        title: string,
        first: string,
        last: string
    }
}

export interface IServiceResult<T> {
    status: ResultStatus,
    data: T
}