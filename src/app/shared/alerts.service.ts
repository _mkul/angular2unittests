import { Injectable } from '@angular/core';
import { AlertTypes } from './enums';

@Injectable()
export class AlertsService {

    alerts = [];

    constructor() {}

    addAlert(message: string, type = AlertTypes.Default) {
        this.alerts.push({ message, type });
    }

    addWarningAlert(message: string) {
        this.addAlert(message, AlertTypes.Warning);
    }

    addSuccessAlert(message: string) {
        this.addAlert(message, AlertTypes.Success);
    }
}
