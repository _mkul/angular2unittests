import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import { Http } from '@angular/http';
import { IServiceResult, IUser } from './interfaces';
import {ResultStatus} from './enums';

@Injectable()
export class ApiService {

  private readonly apiBase = 'https://randomuser.me/api/?results=10';

  constructor (private http: Http) {

  }

  getUsers(): Observable<IServiceResult<IUser[]>> {
    return this.http.get(this.apiBase).map( res => {
      let body = res.json();
      return {
        status: ResultStatus.Ok,
         data: body.results};
    }).catch( err => {
      return Observable.throw({
        status: ResultStatus.ServerError,
        data: []
      });
    });
  }
}
