import {MyCapitalizePipe} from './capitalize.pipe';
let sut: MyCapitalizePipe;

describe('MyCapitalize tests', () => {

    beforeEach(() => {
        sut = new MyCapitalizePipe();
    });

    it('should capitalize value case 1', () => {
        let result = sut.transform('text');

        expect(result).toEqual('Text');
    });

    it('should capitalize value case 2', () => {
        let result = sut.transform('Text');

        expect(result).toEqual('Text');
    });

    it('should capitalize value case 3', () => {
        let result = sut.transform('TEXT');

        expect(result).toEqual('Text');
    });

});
