export enum ResultStatus {
  Ok = 1,
  NotFound,
  NotAllowed,
  ServerError
}

export enum AlertTypes {
  Default = 1,
  Success,
  Info,
  Warning,
  Danger
}
