import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'myCapitalize'})
export class MyCapitalizePipe implements PipeTransform {
  transform(value: string): string {
      let result = value.slice(0, 1).toUpperCase() + value.slice(1).toLowerCase();
      return result;
  }
}
