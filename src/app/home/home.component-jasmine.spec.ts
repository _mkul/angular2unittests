import { Observable } from 'rxjs/Observable';
import { HomeComponent } from './home.component';
import { ApiService } from '../shared/api.service';
import { AlertsService } from '../shared/alerts.service';
import {IServiceResult, IUser} from '../shared/interfaces';
import * as _ from 'lodash';

describe('Home Component', () => {
  let ApiServiceMock: ApiService,
      AlertsServiceMock: AlertsService,
      sut: HomeComponent;

  beforeEach(() => {
    let result: IServiceResult<IUser[]> = { status: 1, data: [
      {name: {title: 'mr', first: 'name1', last: 'last'}},
      {name: {title: 'mr', first: 'name2', last: 'last'}},
      {name: {title: 'mr', first: 'name3', last: 'last'}}
      ]};

    ApiServiceMock = jasmine.createSpyObj<ApiService>('ApiService', ['getUsers']);
    AlertsServiceMock = jasmine.createSpyObj<AlertsService>('AlertsService', ['addSuccessAlert']);

  (<jasmine.Spy>ApiServiceMock.getUsers).and.returnValue(Observable.of(result));

    sut = new HomeComponent(ApiServiceMock, AlertsServiceMock);
  });

  it('Initial test', () => {
    expect(sut.deleteUser).toBeDefined();
  });

  it('should delete appopriate user', () => {
    sut.ngOnInit();
    sut.deleteUser(1);
    expect(sut.users.length).toEqual(2);
    expect(sut.users).toEqual([
      {name: {title: 'mr', first: 'name1', last: 'last'}},
      {name: {title: 'mr', first: 'name3', last: 'last'}}
      ]);
  });

  it('should add success alert', () => {
    sut.ngOnInit();
    sut.deleteUser(1);
    console.log('keys: ', _.keys(ApiService));
    expect(AlertsServiceMock.addSuccessAlert).toHaveBeenCalledTimes(2);
  });

});
