import { Component, OnInit } from '@angular/core';
import {ApiService} from '../shared/api.service';
import {IUser} from '../shared/interfaces';
import {AlertsService} from '../shared/alerts.service';

@Component({
  selector: 'my-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  users: IUser[];

  constructor( private apiService: ApiService, private alertsService: AlertsService) {
  }

  deleteUser(index): void {
    this.users.splice(index, 1);
    this.alertsService.addSuccessAlert('User deleted');
  }

  ngOnInit() {
    this.apiService.getUsers().subscribe(
      result => { this.users = result.data; },
       error => this.alertsService.addWarningAlert('Error')
       );
  }

}
