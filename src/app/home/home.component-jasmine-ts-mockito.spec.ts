import { Observable } from 'rxjs/Observable';
import { HomeComponent } from './home.component';
import { ApiService } from '../shared/api.service';
import { AlertsService } from '../shared/alerts.service';
import {mock, when, instance, verify } from 'ts-mockito';
import {IServiceResult, IUser} from '../shared/interfaces';

describe('Home Component', () => {
  let ApiServiceMock: ApiService,
      mockedAlertsService: any,
      AlertsServiceMock: AlertsService,
      sut: HomeComponent;

  beforeEach(() => {
    let result: IServiceResult<IUser[]> = { status: 1, data: [
      {name: {title: 'mr', first: 'name1', last: 'last'}},
      {name: {title: 'mr', first: 'name2', last: 'last'}},
      {name: {title: 'mr', first: 'name3', last: 'last'}}
      ]};

    let mockedApiService = mock(ApiService);
    mockedAlertsService = mock(AlertsService);
    ApiServiceMock = instance(mockedApiService);
    AlertsServiceMock = instance(mockedAlertsService);

    when(mockedApiService.getUsers()).thenReturn(Observable.of(result));

    sut = new HomeComponent(ApiServiceMock, AlertsServiceMock);
  });

  it('Initial test', () => {
    expect(sut.deleteUser).toBeDefined();
  });

  it('should delete appopriate user', () => {
    sut.ngOnInit();
    sut.deleteUser(1);
    expect(sut.users.length).toEqual(2);
    expect(sut.users).toEqual([
      {name: {title: 'mr', first: 'name1', last: 'last'}},
      {name: {title: 'mr', first: 'name3', last: 'last'}}
      ]);
  });

  it('should add success alert', () => {
    sut.ngOnInit();
    sut.deleteUser(1);
    verify(mockedAlertsService.addSuccessAlert('User deleted')).once();
  });

});
