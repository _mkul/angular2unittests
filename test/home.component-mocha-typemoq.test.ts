import { Observable } from 'rxjs/Observable';
import { HomeComponent } from './home.component';
import { ApiService } from '../shared/api.service';
import { AlertsService } from '../shared/alerts.service';
import * as TypeMoq from 'typemoq';
import {IServiceResult, IUser} from '../shared/interfaces';

describe('Home Component', () => {
  let ApiServiceMock: TypeMoq.Mock<ApiService>,
      AlertsServiceMock: TypeMoq.Mock<AlertsService>,
  sut: HomeComponent;

  beforeEach(() => {
    let result: IServiceResult<IUser[]> = { status: 1, data: [
      {name: {title: 'mr', first: 'name1', last: 'last'}},
      {name: {title: 'mr', first: 'name2', last: 'last'}},
      {name: {title: 'mr', first: 'name3', last: 'last'}}
      ]};

    ApiServiceMock = TypeMoq.Mock.ofType(ApiService);
    AlertsServiceMock = TypeMoq.Mock.ofType(AlertsService);

    ApiServiceMock.setup(x => x.getUsers()).returns(() => Observable.of(result));

    sut = new HomeComponent(ApiServiceMock.object, AlertsServiceMock.object);
  });

  it('Initial test', () => {
    expect(sut.deleteUser).toBeDefined();
  });

  it('should delete appopriate user', () => {
    sut.ngOnInit();
    sut.deleteUser(1);
    expect(sut.users.length).toEqual(2);
  });

  it('should add success alert', () => {
    sut.ngOnInit();
    sut.deleteUser(1);
    AlertsServiceMock.verify(x => x.addSuccessAlert(TypeMoq.It.isValue('User deleted')), TypeMoq.Times.once());
  });

});
